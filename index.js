var WebSocketServer = require('websocket').server;
var http = require('http');
var path = require('path');
var fs = require('fs');

var payloads = new Map([
    ['navio1/message', '{}'],
    ['navio2/message', '{}'],
    ['engine/message', '{}'],
    ['joystick/data' , '{}']
]);

var userSet = new Set();

//HTTP server to serve webpage and process data from POST messages
var server = http.createServer(function(request, response) {
    data = '';
    //Handles POST messages and data
    if (request.method === 'POST') {
        //Receives data from post message and destroys connection if the message is too large
        request.on('data', function (postData) {
            data += postData;
            if (data.length > 1e6) {
                data = "";
                response.writeHead(413, { 'Content-Type': 'text/plain' }).end();
                request.connection.destroy();
            }
        });

        //Triggers once all data has been received
        request.on('end', function () {
            var jsonObj = tryParseJSON(data);
            response.writeHead(200, "OK", { 'Content-Type': 'text/plain' });
            if (!jsonObj) {
                response.write('Data not in JSON format');
            } else if ('topic' in jsonObj && payloads.has(jsonObj['topic']) && payloads.get(jsonObj['topic']) !== data) {
                payloads.set(jsonObj['topic'], data);
                response.write(data);
                updateUsers(jsonObj['topic']);
            } else if ('topic' in jsonObj && payloads.has(jsonObj['topic'])) {
                response.write('Data already up to date');
            } else if (!('topic' in jsonObj)) {
                response.write('Topic not supported');
            }
            response.end();
        });
    //Serves HTML and JS files for webpage
    } else if (request.method === 'GET') {
        var filePath = request.url;
        if (filePath == '/')
            filePath = '/index.html';

        filePath = __dirname + filePath;
        var extname = path.extname(filePath);
        var contentType = 'text/html';

        switch (extname) {
            case '.js':
                contentType = 'text/javascript';
                break;
            case '.css':
                contentType = 'text/css';
                break;
        }

        fs.exists(filePath, function (exists) {
            if (exists) {
                fs.readFile(filePath, function (error, content) {
                    if (error) {
                        console.log('Error reading file');
                        response.writeHead(500);
                        response.end();
                    }
                    else {
                        response.writeHead(200, { 'Content-Type': contentType });
                        response.end(content, 'utf-8');
                    }
                });
            } else {
                console.log('File requested does not exist');
            }
        });
    }
});
server.listen(8080, function () {
    // runs as server starts listening
    console.log('Server initialized');
});

// create the server
wsServer = new WebSocketServer({
    httpServer: server
});

// WebSocket server
wsServer.on('request', function (request) {
    var connection = request.accept(null, request.origin);

    // This is the most important callback for us, we'll handle
    // all messages from users here.
    connection.on('message', function(message) {
        if (message.type === 'utf8') {
            // process WebSocket message
            console.log('Websocket connection received');
            data = tryParseJSON(message.utf8Data);
            if (!data) {
                console.log('Message not in JSON format');
            } else if ('CON' in data) {
                console.log('Sending acknowledgement');
                connection.send(JSON.stringify({ 'CONNACK': 1 }));
                console.log('Sending previous data');
                for (let key of payloads.keys()) {
                    connection.send(payloads.get(key));
                }
                console.log('Adding user ' + connection + ' to userSet');
                userSet.add(connection);
            }
        }
    });

    connection.on('close', function(connection) {
        // close user connection
        console.log('User ' + connection + ' disconnected');
        userSet.delete(connection);
    });
});

function updateUsers(topic) {
    for (let user of userSet) {
        user.send(payloads.get(topic));
    }
}

/*
setInterval(function () {
    console.log('trying to send something');
    for (let user of userSet) {
        user.send(JSON.stringify({test:'val'}));
    }
}, 1000)
*/

function isEmpty(obj) {
    for (var prop in obj) {
        if (obj.hasOwnProperty(prop)) {
            return false;
        }
    }

    return JSON.stringify(obj) === JSON.stringify({});
}

function tryParseJSON(jsonString) {
    try {
        var o = JSON.parse(jsonString);
        if (o && typeof o === "object") {
            return o;
        }
    }
    catch (e) { }
    return false;
};

function getKeys(obj) {
    var keys = [];
    for (var key in obj) {
        keys.push(key);
    }
    return keys;
}